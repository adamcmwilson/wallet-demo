package com.adamwilson.walletdemo.data.model;

import org.junit.Before;
import org.junit.Test;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Created by adam on 30/09/16.
 */
public class CardTest {

    Card card;

    @Before
    public void setUp() throws Exception {
        card = new Card();
    }

    @Test
    public void getPolicyDate_null() throws Exception {
        card.policy_start_date = null;
        card.policy_end_date = null;

        assertNull(card.getPolicyStartDate());
        assertNull(card.getPolicyEndDate());
    }

    @Test
    public void getPolicyDate_empty() throws Exception {
        card.policy_start_date = "";
        card.policy_end_date = "";

        assertNull(card.getPolicyStartDate());
        assertNull(card.getPolicyEndDate());
    }

    @Test
    public void getPolicyDate_invalid() throws Exception {
        card.policy_start_date = "invalid date";
        card.policy_end_date = "invalid date";

        assertNull(card.getPolicyStartDate());
        assertNull(card.getPolicyEndDate());

    }

    @Test
    public void getPolicyDate_valid() throws Exception {
        card.policy_start_date = "2016-04-21T04:00:00Z";
        card.policy_end_date = "2017-01-01T05:00:00Z";

        Date start = card.getPolicyStartDate();
        assertNotNull(start);

        Date end = card.getPolicyEndDate();
        assertNotNull(end);

        Calendar calendar = new GregorianCalendar();

        calendar.setTime(start);
        assertEquals(2016, calendar.get(Calendar.YEAR));
        assertEquals(Calendar.APRIL, calendar.get(Calendar.MONTH));
        assertEquals(21, calendar.get(Calendar.DAY_OF_MONTH));
        assertEquals(4, calendar.get(Calendar.HOUR));
        assertEquals(0, calendar.get(Calendar.MINUTE));
        assertEquals(0, calendar.get(Calendar.SECOND));

        calendar.setTime(end);
        assertEquals(2017, calendar.get(Calendar.YEAR));
        assertEquals(Calendar.JANUARY, calendar.get(Calendar.MONTH));
        assertEquals(1, calendar.get(Calendar.DAY_OF_MONTH));
        assertEquals(5, calendar.get(Calendar.HOUR));
        assertEquals(0, calendar.get(Calendar.MINUTE));
        assertEquals(0, calendar.get(Calendar.SECOND));
    }
}