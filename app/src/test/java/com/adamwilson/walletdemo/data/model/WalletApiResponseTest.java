package com.adamwilson.walletdemo.data.model;

import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

public class WalletApiResponseTest {

    Moshi moshi;
    JsonAdapter<WalletApiResponse> adapter;

    @Before
    public void setUp() throws Exception {
        moshi = new Moshi.Builder().build();
        adapter = moshi.adapter(WalletApiResponse.class);
    }

    @Test
    public void testParseSampleResponse() throws Exception {
        WalletApiResponse result = adapter.fromJson(getJson());

        assertNotNull(result);
        assertNotNull(result.info);

        assertNotNull(result.info.message_type);
        assertEquals("wallet", result.info.message_type);

        assertNotNull(result.info.cards);
        assertFalse(result.info.cards.isEmpty());
        assertEquals(5, result.info.cards.size());

        Card one = result.info.cards.get(0);
        assertNotNull(one);
        assertEquals(1, one.id);
        assertEquals("personal_spending_account", one.type);
        assertEquals(300, one.amount, 0);
        assertEquals("CAD", one.currency);
        assertEquals("2017-01-01T05:00:00Z", one.policy_end_date);
        assertEquals("2016-04-21T04:00:00Z", one.policy_start_date);
    }

    private String getJson() {
        return "{\"info\":{" +
                "\"message_type\":\"wallet\"," +
                "\"cards\":[{" +
                "  \"id\": 1," +
                "  \"type\":\"personal_spending_account\"," +
                "  \"amount\":300," +
                "  \"currency\":\"CAD\"," +
                "  \"policy_end_date\":\"2017-01-01T05:00:00Z\"," +
                "  \"policy_start_date\":\"2016-04-21T04:00:00Z\"" +
                "}, {" +
                "  \"id\": 2," +
                "  \"type\":\"health_spending_account\"," +
                "  \"amount\":710," +
                "  \"currency\":\"CAD\"," +
                "  \"policy_end_date\":\"2017-01-01T05:00:00Z\"," +
                "  \"policy_start_date\":\"2016-04-21T04:00:00Z\"" +
                "}, {" +
                "  \"id\": 3," +
                "  \"type\":\"personal_store_credit\"," +
                "  \"amount\":50," +
                "  \"currency\":\"CAD\"," +
                "  \"policy_end_date\":\"2017-01-01T05:00:00Z\"," +
                "  \"policy_start_date\":\"2016-04-21T04:00:00Z\"" +
                "}, {" +
                "  \"id\": 4," +
                "  \"type\":\"health_spending_account\"," +
                "  \"amount\":203," +
                "  \"currency\":\"CAD\"," +
                "  \"policy_end_date\":\"2018-01-01T05:00:00Z\"," +
                "  \"policy_start_date\":\"2017-04-21T04:00:00Z\"" +
                "}, {" +
                "  \"id\": 5," +
                "  \"type\":\"personal_spending_account\"," +
                "  \"amount\":833," +
                "  \"currency\":\"CAD\"," +
                "  \"policy_end_date\":\"2017-01-01T05:00:00Z\"," +
                "  \"policy_start_date\":\"2016-10-21T04:00:00Z\"" +
                "}]}}";
    }


}