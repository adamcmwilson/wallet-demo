package com.adamwilson.walletdemo.view;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.adamwilson.walletdemo.data.model.Card;
import com.adamwilson.walletdemo.data.model.Info;
import com.adamwilson.walletdemo.walletdemo.R;

import rx.Observer;

public class CardListLayout extends FrameLayout implements Observer<Info> {

    RecyclerView recycler_view;
    RecyclerView.Adapter adapter;
    GridLayoutManager layout_manager;

    public CardListLayout(Context context) {
        this(context, null);
    }

    public CardListLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CardListLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        View view = LayoutInflater.from(context).inflate(R.layout.card_list_layout, this, false);
        this.addView(view);

        layout_manager = new GridLayoutManager(context, attrs, defStyleAttr, 0);
        layout_manager.setSpanCount(getResources().getInteger(R.integer.card_column_count));
        recycler_view = (RecyclerView) view.findViewById(R.id.recycler_view);
        recycler_view.setLayoutManager(layout_manager);
    }

    @Override
    public void onNext(Info info) {
        if (adapter == null) adapter = new CardListAdapter(info);
        recycler_view.setAdapter(adapter);
    }

    @Override
    public void onError(Throwable e) {
        e.printStackTrace();
    }

    @Override
    public void onCompleted() {

    }

    class CardListAdapter extends RecyclerView.Adapter<CardView.Presenter> {

        Info info;

        public CardListAdapter(Info info) {
            this.info = info;
        }

        @Override
        public CardView.Presenter onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_list_item, parent, false);
            return new CardView.Presenter(view);
        }

        @Override
        public void onBindViewHolder(CardView.Presenter holder, int position) {
            Card card = getItem(position);
            if (card == null) return;
            holder.bind(card);
        }

        @Override
        public int getItemCount() {
            if (info == null || info.cards == null || info.cards.isEmpty()) return 0;
            return info.cards.size();
        }

        private Card getItem(final int pos) {
            if (info == null || info.cards == null || info.cards.isEmpty()) return null;
            return info.cards.get(pos);
        }
    }
}
