package com.adamwilson.walletdemo.view;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.adamwilson.walletdemo.data.model.Card;
import com.adamwilson.walletdemo.walletdemo.R;

public class CardView extends RelativeLayout {

    public CardView(Context context) {
        this(context, null);
    }

    public CardView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CardView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public static class Presenter extends RecyclerView.ViewHolder {

        TextView text_card_type;
        TextView text_balance;
        TextView text_effective_dates;

        Presenter(View itemView) {
            super(itemView);
            text_card_type = (TextView) itemView.findViewById(R.id.text_card_type);
            text_balance = (TextView) itemView.findViewById(R.id.text_balance);
            text_effective_dates = (TextView) itemView.findViewById(R.id.text_effective_dates);
        }

        public void bind(Card card) {
            Context context = text_card_type.getContext();

            text_card_type.setText(card.getTypeForDisplay(context));
            text_balance.setText(context.getString(R.string.balance_display, card.amount));

            text_effective_dates.setText(
                    context.getString(
                            R.string.effective_date_display,
                            card.getPolicyStartDate(),
                            card.getPolicyEndDate()));
        }

    }
}
