package com.adamwilson.walletdemo.data.model;

import java.util.List;

public class Info {

    public String message_type;
    public List<Card> cards;

    @Override
    public String toString() {
        return "Info{" +
                "message_type='" + message_type + '\'' +
                ", cards=" + cards +
                '}';
    }
}
