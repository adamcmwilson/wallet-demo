package com.adamwilson.walletdemo.data.model;

import android.content.Context;

import com.adamwilson.walletdemo.walletdemo.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Card {

    public static final String card_personal_spending_account = "personal_spending_account";
    public static final String card_health_spending_account = "health_spending_account";
    public static final String card_personal_store_credit = "personal_store_credit";

    public int id;
    public String type;
    public double amount;
    public String currency;
    public String policy_start_date;
    public String policy_end_date;

    public Date getPolicyStartDate() {
        return getDateFrom(policy_start_date);
    }

    public Date getPolicyEndDate() {
        return getDateFrom(policy_end_date);
    }

    /**
     * @return the expected date format for this object.
     */
    DateFormat getDateFormat() {
        return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
    }

    /**
     * Internal method to parse policy dates from provided string values.
     * @param date_string provided from the Wallet API
     * @return a valid date, or null.
     */
    private Date getDateFrom(String date_string) {
        if (date_string == null) return null;
        if (date_string.isEmpty()) return null;

        try {
            return getDateFormat().parse(date_string);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Retrieve a localized display value for the Card type based on the raw type value
     * @param context to access resources
     */
    public String getTypeForDisplay(Context context) {

        int string_res_id = R.string.card_unknown;

        switch (type) {
            case card_personal_spending_account:
                string_res_id = R.string.card_personal_spending_account;
                break;
            case card_health_spending_account:
                string_res_id = R.string.card_health_spending_account;
                break;
            case card_personal_store_credit:
                string_res_id = R.string.card_personal_store_credit;
                break;
        }

        return context.getString(string_res_id);
    }

    @Override
    public String toString() {
        return "Card{" +
                "id=" + id +
                ", type='" + type + '\'' +
                ", amount=" + amount +
                ", currency='" + currency + '\'' +
                ", policy_start_date='" + policy_start_date + '\'' +
                ", policy_end_date='" + policy_end_date + '\'' +
                '}';
    }


}
