package com.adamwilson.walletdemo.data.model;

public class WalletApiResponse {

    public Info info;

    @Override
    public String toString() {
        return "WalletApiResponse{" +
                "info=" + info +
                '}';
    }
}
