package com.adamwilson.walletdemo.data.api;

import com.adamwilson.walletdemo.data.model.WalletApiResponse;

import rx.Observable;

public interface WalletApi {

    Observable<WalletApiResponse> observeWalletApiResponse();

}
