package com.adamwilson.walletdemo.data.api;

import com.adamwilson.walletdemo.data.model.WalletApiResponse;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import rx.Observable;
import rx.functions.Action0;
import rx.subjects.PublishSubject;

public class GistWalletApi implements WalletApi {

    OkHttpClient client = new OkHttpClient();
    PublishSubject<WalletApiResponse> api_response_subject = PublishSubject.create();

    @Override
    public Observable<WalletApiResponse> observeWalletApiResponse() {
        return api_response_subject
                .asObservable()
                .doOnSubscribe(new Action0() {
                    @Override
                    public void call() {
                        fetch();
                    }
                });
    }

    private void fetch() {
        Request request = new Request.Builder()
                .url("https://gist.githubusercontent.com/Shanjeef/3562ebc5ea794a945f723de71de1c3ed/raw/25da03b403ffa860dd68a9bfc84f562262ee5ca5/walletEndpoint")
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try {

                    if (!response.isSuccessful())
                        api_response_subject.onError(
                                new IOException("Unexpected code " + response));


                    final JsonAdapter<WalletApiResponse> adapter =
                            new Moshi.Builder().build().adapter(WalletApiResponse.class);
                    final WalletApiResponse mock_response = adapter.fromJson(response.body().string());

                    api_response_subject.onNext(mock_response);

                } catch (Exception e) {
                    api_response_subject.onError(e);
                }


            }
        });
    }
}
