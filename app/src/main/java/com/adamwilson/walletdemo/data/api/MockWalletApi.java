package com.adamwilson.walletdemo.data.api;

import android.content.Context;

import com.adamwilson.walletdemo.data.model.WalletApiResponse;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;

import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;

import okio.BufferedSource;
import okio.Okio;
import okio.Source;
import rx.Observable;

/**
 * This is a mock data provider to allow for UI development and testing.
 * The data source is: assets/mock_wallet_api.json
 */
public class MockWalletApi implements WalletApi {

    private final String source_json_file = "mock_wallet_api.json";

    WeakReference<Context> context;

    public MockWalletApi(Context context) {
        this.context = new WeakReference<>(context);
    }

    @Override
    public Observable<WalletApiResponse> observeWalletApiResponse() {

        try {

            // open the asset file:
            final InputStream json_file_input_stream = context.get().getAssets().open(source_json_file);

            // buffer the stream:
            final Source json_file_source = Okio.source(json_file_input_stream);
            final BufferedSource buffered_json_file_source = Okio.buffer(json_file_source);

            // parse the json using Moshi:
            final JsonAdapter<WalletApiResponse> adapter =
                    new Moshi.Builder().build().adapter(WalletApiResponse.class);
            final WalletApiResponse mock_response = adapter.fromJson(buffered_json_file_source);

            json_file_input_stream.close();
            return Observable.just(mock_response);

        } catch (IOException e) {
            e.printStackTrace();
            return Observable.error(e);
        }

    }
}
