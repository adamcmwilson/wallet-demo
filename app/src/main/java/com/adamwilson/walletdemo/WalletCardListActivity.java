package com.adamwilson.walletdemo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.adamwilson.walletdemo.data.api.GistWalletApi;
import com.adamwilson.walletdemo.data.api.MockWalletApi;
import com.adamwilson.walletdemo.data.api.WalletApi;
import com.adamwilson.walletdemo.data.model.Info;
import com.adamwilson.walletdemo.data.model.WalletApiResponse;
import com.adamwilson.walletdemo.view.CardListLayout;
import com.adamwilson.walletdemo.walletdemo.R;

import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class WalletCardListActivity extends AppCompatActivity {

    WalletApi api;
    CompositeSubscription subscriptions;

    CardListLayout card_ist_view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet_card_list);

        card_ist_view = (CardListLayout) findViewById(R.id.card_list_view);

        subscriptions = new CompositeSubscription();

//        api = new MockWalletApi(this);
        api = new GistWalletApi();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (subscriptions != null && !subscriptions.isUnsubscribed()){
            subscriptions.unsubscribe();
            subscriptions.clear();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        subscribeWalletApi();
    }

    private void subscribeWalletApi() {
        if (api == null) return;

        subscriptions.add(
                api.observeWalletApiResponse()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .filter(new Func1<WalletApiResponse, Boolean>() {
                            @Override
                            public Boolean call(WalletApiResponse walletApiResponse) {
                                return walletApiResponse != null
                                        && walletApiResponse.info != null;
                            }
                        })
                        .map(new Func1<WalletApiResponse, Info>() {
                            @Override
                            public Info call(WalletApiResponse walletApiResponse) {
                                return walletApiResponse.info;
                            }
                        })
                        .subscribe(card_ist_view)
        );
    }
}
